def get_cs():
    cs=input("enter the string\n")
    return cs
def cs_to_dict(cs):
    d = {}
    for i in cs.split(";"):
        k,v = i.split('=')
        d[k]=v
    return d
def dict_to_cs(d):
    cs = ""
    for i in d.items():
        cs+=i[0]+"="+i[1]+";"
    return cs.strip(";")
def main():
    cs=get_cs()
    d=cs_to_dict(cs)
    print(d)
    cs=dict_to_cs(d)
    print("Using def dict_to_cs is:\n",cs)
main()